<?php

namespace designerei\ContaoAspectRatioExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoAspectRatioExtendedListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ($template->type === 'player' || $template->type === 'youtube' || $template->type === 'vimeo') {
            $template->aspect = str_replace(':', '', $template->playerAspect);
        } elseif ($template->type === 'image') {
            $template->aspect = str_replace(':', '', $template->imgAspect);
        }
    }
}
