<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['playerAspect']['sql'] = "char(5) NOT NULL default '3:2'";

$GLOBALS['TL_DCA']['tl_content']['fields']['imgAspect'] = [
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('16:9', '16:10', '21:9', '4:3', '3:2', '1:1'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_content']['player_aspect'],
    'eval'                    => array('includeBlankOption' => true, 'nospace'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(8) NOT NULL default ''"
];

PaletteManipulator::create()
  ->addField('playerAspect', 'playerSize', PaletteManipulator::POSITION_AFTER)
  ->applyToPalette('player', 'tl_content')
  ->addField('imgAspect', 'size', PaletteManipulator::POSITION_AFTER)
  ->applyToPalette('image', 'tl_content')
;
